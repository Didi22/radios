package com.example.radios;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StationService {

    @GET("stations/byuuid/{stationuuid}")
    Call<List<Station>> getOne(@Path("stationuuid") String stationuuid);

    @GET("stations")
    Call<List<Station>> getStationsList();

    @GET("stations/bycountry/{country}")
    Call<List<Station>> getCountries(@Path("country") String country);

    @GET("stations/search{id}")
    Call<List<Station>> getCountriesStyle(@Path("id") String id);

    @GET("stations/byname/{nom}")
    Call<List<Station>> getNom(@Path("nom") String nom);
}
