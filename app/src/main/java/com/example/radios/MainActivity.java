package com.example.radios;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ListActivity;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String BASE_URL_RADIO = "http://fr1.api.radio-browser.info./json/";
    String url;
    ArrayList<Station> station;
    ListView lvListe;
    EditText saisieCountry;
    EditText saisieStyle;
    EditText saisieNom;
    String reponseCountry;
    String reponseStyle;
    String reponseNom;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getOneStation();
        getCountriesStyle();
        Button go = ((Button) findViewById(R.id.buttonGo));
        go.setOnClickListener(view -> {
            getCountriesStyle();
        });

        Button goNom = ((Button) findViewById(R.id.buttonGoNom));
        goNom.setOnClickListener(view -> {
            getNom();
        });

        Button b1 = ((Button) findViewById(R.id.button));
        b1.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.button:
            case R.id.lvListe:
                //Bundle bundle = new Bundle();
                //  bundle.putParcelableArrayList("STATION", station);
                intent = new Intent(this, MainActivity2.class);
                //  intent.putExtras(bundle);
                intent.putExtra("URL", url);
                Log.v("url3", url);
                startActivity(intent);
                break;
            case R.id.buttonGo:
                saisieCountry = ((EditText) findViewById(R.id.edittextCountry));
                reponseCountry = saisieCountry.getText().toString();
                Log.v("country", reponseCountry);

                saisieStyle = ((EditText) findViewById(R.id.edittextStyle));
                reponseStyle = saisieStyle.getText().toString();
                Log.v("style", reponseStyle);
                break;
            case R.id.buttonGoNom:
                saisieNom = ((EditText) findViewById(R.id.edittextNom));
                reponseNom = saisieNom.getText().toString();
                Log.v("nom", reponseNom);
                break;
        }
    }

    private void getNom() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RADIO)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        StationService userServiceService = retrofit.create(StationService.class);

        Call<List<Station>> call = userServiceService.getCountriesStyle("?name=" + reponseNom);

        call.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) { //reception de la reponse

                if (response.isSuccessful()) {
                    Log.v("response", "" + response.body());
                    Log.v("code resp", "" + response.code());
                    Log.v("raw resp", "" + response.raw().networkResponse());
                    station = (ArrayList<Station>) response.body();
                    Log.v("station", "" + station);
                    saisieNom = ((EditText) findViewById(R.id.edittextNom));
                    reponseNom = saisieNom.getText().toString();
                    Log.v("nom", reponseNom);


                    setDataStation();

                } else {
                    switch (response.code()) {
                        case 404:
                            Log.v("error", "not found");
                            break;
                        case 500:
                            Log.v("error", "server side error");
                            break;
                        default:
                            Log.v("error", "unknown error");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {
                Log.v("response", t.getMessage());
            }
        });
    }

    private void getCountriesStyle() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RADIO)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        StationService userServiceService = retrofit.create(StationService.class);

        Call<List<Station>> call = userServiceService.getCountriesStyle("?name=" + reponseStyle + "&countries=" + reponseCountry);
       //Call<List<Station>> call = userServiceService.getCountriesStyle("?name=rock&countries=france");

        call.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) { //reception de la reponse

                if (response.isSuccessful()) {
                    Log.v("response", "" + response.body());
                    Log.v("code resp", "" + response.code());
                    Log.v("raw resp", "" + response.raw().networkResponse());
                    station = (ArrayList<Station>) response.body();
                    Log.v("station", "" + station);
                    saisieCountry = ((EditText) findViewById(R.id.edittextCountry));
                    reponseCountry = saisieCountry.getText().toString();
                    Log.v("country", reponseCountry);

                    saisieStyle = ((EditText) findViewById(R.id.edittextStyle));
                    reponseStyle = saisieStyle.getText().toString();
                    Log.v("style", reponseStyle);
                    setDataStation();

                } else {
                    switch (response.code()) {
                        case 404:
                            Log.v("error", "not found");
                            break;
                        case 500:
                            Log.v("error", "server side error");
                            break;
                        default:
                            Log.v("error", "unknown error");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {
                Log.v("response", t.getMessage());
            }
        });
    }

    private void setDataStation() {
        lvListe = (ListView) findViewById(R.id.lvListe);
        StationAdapter adapter = new StationAdapter(this, station); //this correspond a l'activite
        lvListe.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lvListe.setOnItemClickListener((parent, view, position, id) -> {
            Station radio = (Station) lvListe.getItemAtPosition(position);
            String uuid = radio.getUuid();
            Log.v("uuidApp", uuid);
            String name = radio.getName();
            String country = radio.getCountry();
            url = radio.getUrl();
            Log.v("url", url);
            String icon = radio.getFavicon();
        });
    }









    private void getOneStation() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RADIO)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        StationService userServiceService = retrofit.create(StationService.class);

        Call<List<Station>> call = userServiceService.getOne("960e57c5-0601-11e8-ae97-52543be04c81");

        call.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) {

                if (response.isSuccessful()) {
                    Log.v("response", "" + response.body());
                    Log.v("code resp", "" + response.code());
                    Log.v("raw resp", "" + response.raw().networkResponse());
                    station = (ArrayList<Station>) response.body();
                    Log.v("station", "" + station);
                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Log.v("error", "not found");
                            //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Log.v("error", "server side error");
                            // Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Log.v("error", "unknown error");
                            //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {
                Log.v("response", t.getMessage());
            }
        });
    }

    private void getOneTags() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RADIO)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        TagsService userServiceService = retrofit.create(TagsService.class);

        Call<List<Tags>> call = userServiceService.getOne("tags");

        call.enqueue(new Callback<List<Tags>>() {
            @Override
            public void onResponse(Call<List<Tags>> call, Response<List<Tags>> response) {

                if (response.isSuccessful()) {
                    Log.v("response", "" + response.body());
                    Log.v("code resp", "" + response.code());
                    Log.v("raw resp", "" + response.raw().networkResponse());
                    List<Tags> tags = response.body();
                    Log.v("station", "" + tags);
                } else {
                    switch (response.code()) {
                        case 404:
                            Log.v("error", "not found");
                            break;
                        case 500:
                            Log.v("error", "server side error");
                            break;
                        default:
                            Log.v("error", "unknown error");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<List<Tags>> call, Throwable t) {
                Log.v("response", t.getMessage());
            }
        });
    }

    private void getCountries() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RADIO)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        StationService userServiceService = retrofit.create(StationService.class);

        Call<List<Station>> call = userServiceService.getCountries("france");

        call.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) {

                if (response.isSuccessful()) {
                    Log.v("response", "" + response.body());
                    Log.v("code resp", "" + response.code());
                    Log.v("raw resp", "" + response.raw().networkResponse());
                    station = (ArrayList<Station>) response.body();
                    Log.v("station", "" + station);
                } else {
                    switch (response.code()) {
                        case 404:
                            Log.v("error", "not found");
                            break;
                        case 500:
                            Log.v("error", "server side error");
                            break;
                        default:
                            Log.v("error", "unknown error");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {
                Log.v("response", t.getMessage());
            }
        });
    }
}