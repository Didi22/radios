package com.example.radios;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StationAdapter extends BaseAdapter {

    ArrayList< Station > biblio;
    LayoutInflater inflater;
    public StationAdapter(Context context, ArrayList < Station > biblio) {
        inflater = LayoutInflater.from(context);
        this.biblio = biblio;
    }
    @Override
    public int getCount() {
        return biblio.size();
    }
    @Override
    public Object getItem(int position) {
        return biblio.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    private class ViewHolder {
        TextView tvName;
        TextView tvURL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.itemstation, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvURL = (TextView) convertView.findViewById(R.id.tvURL);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvName.setText(biblio.get(position).getName());
        holder.tvURL.setText(biblio.get(position).getUrl());

        return convertView;
    }
}