package com.example.radios;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TagsService {

    @GET("{order}")
    Call<List<Tags>> getOne(@Path("order") String order);
}
