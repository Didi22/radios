package com.example.radios;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;

public class Station implements Parcelable {

    @SerializedName("stationuuid")
    private String uuid;

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("url_resolved")
    private String urlResolved;

    @SerializedName("homepage")
    private String homepage;

    @SerializedName("favicon")
    private String favicon;

    @SerializedName("tags")
    private String tags;

    @SerializedName("country")
    private String country;

    @SerializedName("state")
    private String state;

    @SerializedName("language")
    private String language;

    @SerializedName("votes")
    private Integer votes;

    @SerializedName("codec")
    private String codec;

    @SerializedName("bitrate")
    private Integer bitRate;

    @SerializedName("hls")
    private Integer hls;

    @SerializedName("lastcheckok")
    private Integer lastCheckCk;

    @SerializedName("clickcount")
    private Integer clickCount;

    @SerializedName("clicktrend")
    private Integer clickTrend;

    protected Station(Parcel in) {
        uuid = in.readString();
        name = in.readString();
        url = in.readString();
        urlResolved = in.readString();
        homepage = in.readString();
        favicon = in.readString();
        tags = in.readString();
        country = in.readString();
        state = in.readString();
        language = in.readString();
        if (in.readByte() == 0) {
            votes = null;
        } else {
            votes = in.readInt();
        }
        codec = in.readString();
        if (in.readByte() == 0) {
            bitRate = null;
        } else {
            bitRate = in.readInt();
        }
        if (in.readByte() == 0) {
            hls = null;
        } else {
            hls = in.readInt();
        }
        if (in.readByte() == 0) {
            lastCheckCk = null;
        } else {
            lastCheckCk = in.readInt();
        }
        if (in.readByte() == 0) {
            clickCount = null;
        } else {
            clickCount = in.readInt();
        }
        if (in.readByte() == 0) {
            clickTrend = null;
        } else {
            clickTrend = in.readInt();
        }
    }

    public static final Creator<Station> CREATOR = new Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlResolved() {
        return urlResolved;
    }

    public void setUrlResolved(String urlResolved) {
        this.urlResolved = urlResolved;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getFavicon() {
        return favicon;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public String getCodec() {
        return codec;
    }

    public void setCodec(String codec) {
        this.codec = codec;
    }

    public Integer getBitRate() {
        return bitRate;
    }

    public void setBitRate(Integer bitRate) {
        this.bitRate = bitRate;
    }

    public Integer getHls() {
        return hls;
    }

    public void setHls(Integer hls) {
        this.hls = hls;
    }

    public Integer getLastCheckCk() {
        return lastCheckCk;
    }

    public void setLastCheckCk(Integer lastCheckCk) {
        this.lastCheckCk = lastCheckCk;
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    public Integer getClickTrend() {
        return clickTrend;
    }

    public void setClickTrend(Integer clickTrend) {
        this.clickTrend = clickTrend;
    }

    @Override
    public String toString() {
        return "Station{" +
                "uuid='" + uuid + '\'' +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", urlResolved='" + urlResolved + '\'' +
                ", homepage='" + homepage + '\'' +
                ", favicon='" + favicon + '\'' +
                ", tags='" + tags + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", language='" + language + '\'' +
                ", votes=" + votes +
                ", codec='" + codec + '\'' +
                ", bitRate=" + bitRate +
                ", hls=" + hls +
                ", lastCheckCk=" + lastCheckCk +
                ", clickCount=" + clickCount +
                ", clickTrend=" + clickTrend +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uuid);
        dest.writeString(name);
        dest.writeString(url);
        dest.writeString(urlResolved);
        dest.writeString(homepage);
        dest.writeString(favicon);
        dest.writeString(tags);
        dest.writeString(country);
        dest.writeString(state);
        dest.writeString(language);
        if (votes == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(votes);
        }
        dest.writeString(codec);
        if (bitRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bitRate);
        }
        if (hls == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(hls);
        }
        if (lastCheckCk == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lastCheckCk);
        }
        if (clickCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(clickCount);
        }
        if (clickTrend == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(clickTrend);
        }
    }
}
