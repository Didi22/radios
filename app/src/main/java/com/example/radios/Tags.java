package com.example.radios;

import com.google.gson.annotations.SerializedName;

public class Tags {

    @SerializedName("name")
    private String name;

    @SerializedName("stationcount")
    private int stationcount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStationcount() {
        return stationcount;
    }

    public void setStationcount(int stationcount) {
        this.stationcount = stationcount;
    }

    @Override
    public String toString() {
        return "Tags{" +
                "name='" + name + '\'' +
                ", stationcount=" + stationcount +
                '}';
    }
}

