package com.example.radios;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity {

    static String url;
    Button buttonPlay;
    Button buttonPause;
    Button buttonStop;

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        buttonPlay = ((Button) findViewById(R.id.buttonPlay));
        buttonPause = ((Button) findViewById(R.id.buttonPause));
      //  buttonStop = ((Button) findViewById(R.id.buttonStop));

        TextView t = (TextView) findViewById(R.id.textView);
        Bundle extras = getIntent().getExtras();
        url = extras.getString("URL");
        t.setText(url);

        try {
            MediaPlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void MediaPlayer() throws IOException {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioAttributes(
                new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
        );
        mediaPlayer.setDataSource(url);
        mediaPlayer.prepare();
        mediaPlayer.start();


        buttonPlay.setOnClickListener(view -> {

            if (mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            } else {
                mediaPlayer.start();
            }
        });

        buttonPause.setOnClickListener(view -> {

            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.start();
            }
        });

        /*buttonStop.setOnClickListener(view -> {

            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            } else {
                mediaPlayer.start();
            }
        });*/

    }
}